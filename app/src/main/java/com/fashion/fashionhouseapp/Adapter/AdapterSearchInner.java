package com.fashion.fashionhouseapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouseapp.R;


public class AdapterSearchInner extends RecyclerView.Adapter<AdapterSearchInner.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private String [] listSearch = {"Brand A-Z", "Peperempe", "Trap paris", "Dice & Joker", "Fiftygrind", "Pull & bear"};

    public AdapterSearchInner(Context mContext, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_search_inner, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.textSearch.setText(listSearch[position]);

    }

    @Override
    public int getItemCount() {
        return listSearch.length;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView textSearch;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            textSearch = itemView.findViewById(R.id.textSearch);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}