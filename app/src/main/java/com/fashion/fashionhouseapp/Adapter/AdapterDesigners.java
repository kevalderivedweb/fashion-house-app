package com.fashion.fashionhouseapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fashion.fashionhouseapp.R;


public class AdapterDesigners extends RecyclerView.Adapter<AdapterDesigners.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private String[] name = {"A", "B", "C", "D", "E", "F"};
    private AdapterDesignerSub adapterDesignerSub;


    public AdapterDesigners(Context mContext, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_designers, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        holder.headerText.setText(name[position]);


        holder.recListDesignerSub.setLayoutManager(new LinearLayoutManager(mContext));
        adapterDesignerSub = new AdapterDesignerSub(mContext, position, new AdapterDesignerSub.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                listener.onItemClick(position, item);
            }
        });
        holder.recListDesignerSub.setAdapter(adapterDesignerSub);


    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView headerText;
        RecyclerView recListDesignerSub;


        public Viewholder(@NonNull View itemView) {
            super(itemView);

            headerText = itemView.findViewById(R.id.headerText);
            recListDesignerSub = itemView.findViewById(R.id.recListDesignerSub);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, int item);
    }
}