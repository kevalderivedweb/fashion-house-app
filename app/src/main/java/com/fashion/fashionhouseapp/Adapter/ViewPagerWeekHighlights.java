package com.fashion.fashionhouseapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;

import com.fashion.fashionhouseapp.Model.ServiceModel;
import com.fashion.fashionhouseapp.R;

import java.util.ArrayList;

public class ViewPagerWeekHighlights extends PagerAdapter {
    Context mContext;
    ArrayList<ServiceModel> McategoryModels;
    private final OnItemClickListener listener;

    public ViewPagerWeekHighlights(Context context, ArrayList<ServiceModel> categoryModels, OnItemClickListener listener) {
        this.mContext = context;
        this.McategoryModels = categoryModels;
        this.listener = listener;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.view_pager_week_highlights, container, false);


        container.addView(layout);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 5;
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}
