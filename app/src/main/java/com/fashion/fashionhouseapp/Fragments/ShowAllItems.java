package com.fashion.fashionhouseapp.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fashion.fashionhouseapp.Activity.ItemsDetail;
import com.fashion.fashionhouseapp.Activity.Refine;
import com.fashion.fashionhouseapp.Adapter.AdapterDesignerInner;
import com.fashion.fashionhouseapp.R;

public class ShowAllItems extends AppCompatActivity {

    private RecyclerView recShowAll;
    private AdapterDesignerInner adapterDesignerInner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all_items);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        recShowAll = findViewById(R.id.recShowAll);
        recShowAll.setLayoutManager(new GridLayoutManager(ShowAllItems.this, 2));
        adapterDesignerInner = new AdapterDesignerInner(ShowAllItems.this, new AdapterDesignerInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(ShowAllItems.this, ItemsDetail.class));

            }
        });
        recShowAll.setAdapter(adapterDesignerInner);


        findViewById(R.id.refineDesigners).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShowAllItems.this, Refine.class));
            }
        });



    }

}