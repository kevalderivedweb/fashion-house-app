package com.fashion.fashionhouseapp.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fashion.fashionhouseapp.Adapter.AdapterHorizontalHome;
import com.fashion.fashionhouseapp.Adapter.AdapterNewIn;
import com.fashion.fashionhouseapp.Adapter.AdapterRecommended;
import com.fashion.fashionhouseapp.Adapter.ViewPagerRecommended;
import com.fashion.fashionhouseapp.Adapter.ViewPagerWeek;
import com.fashion.fashionhouseapp.Adapter.ViewPagerWeekHighlights;
import com.fashion.fashionhouseapp.Model.ServiceModel;
import com.fashion.fashionhouseapp.R;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;

public class Home extends Fragment {

    private DotsIndicator indicator, worm_dots_indicator2;
    private ViewPagerRecommended viewPagerRecommended;
    private ViewPagerWeek viewPagerWeek;
    private ArrayList<ServiceModel> mDatasetServices = new ArrayList<>();

    private RecyclerView recNewIn;
    private AdapterRecommended adapterRecommended;
    private AdapterHorizontalHome adapterHorizontalHome;
    private AdapterNewIn adapterNewIn;

    private RecyclerView recHome;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_home, container, false);





        recHome = view.findViewById(R.id.recHome);
        recHome.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapterNewIn = new AdapterNewIn(getContext(), new AdapterNewIn.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recHome.setAdapter(adapterNewIn);




        // for new items recycler
        recNewIn = view.findViewById(R.id.recNewIn);
        recNewIn.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        adapterHorizontalHome = new AdapterHorizontalHome(getContext(), new AdapterHorizontalHome.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recNewIn.setAdapter(adapterHorizontalHome);






        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
        indicator = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator);
        viewPagerRecommended = new ViewPagerRecommended(getContext(), mDatasetServices, new ViewPagerRecommended.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        mViewPager.setAdapter(viewPagerRecommended);
        indicator.setViewPager(mViewPager);




        // for viewpager 2
        ViewPager viewPager2 = (ViewPager) view.findViewById(R.id.viewPager2);
        worm_dots_indicator2 = (DotsIndicator) view.findViewById(R.id.worm_dots_indicator2);
        viewPagerWeek = new ViewPagerWeek(getContext(), mDatasetServices, new ViewPagerWeek.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        viewPager2.setAdapter(viewPagerWeek);
        worm_dots_indicator2.setViewPager(viewPager2);




        return view;

    }




}