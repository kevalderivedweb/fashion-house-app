package com.fashion.fashionhouseapp.Fragments;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.fashion.fashionhouseapp.Activity.Designers;
import com.fashion.fashionhouseapp.Adapter.AdapterSearch;
import com.fashion.fashionhouseapp.R;

public class Search extends Fragment {

    private RecyclerView recSearchFrag;
    private AdapterSearch adapterSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_search, container, false);


        recSearchFrag = view.findViewById(R.id.recSearchFrag);
        recSearchFrag.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterSearch = new AdapterSearch(getContext(), new AdapterSearch.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                SearchInner searchInner = new SearchInner();
                replaceFragment(R.id.fragmentLinearHome, searchInner, "search_inner", null);
            }
        });
        recSearchFrag.setAdapter(adapterSearch);


        return view;
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }


}