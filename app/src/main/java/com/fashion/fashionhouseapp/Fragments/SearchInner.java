package com.fashion.fashionhouseapp.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fashion.fashionhouseapp.Adapter.AdapterSearchInner;
import com.fashion.fashionhouseapp.R;

public class SearchInner extends Fragment {

    private RecyclerView recSearchInner;
    private AdapterSearchInner adapterSearchInner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_search_inner, container, false);


        recSearchInner = view.findViewById(R.id.recSearchInner);
        recSearchInner.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterSearchInner = new AdapterSearchInner(getContext(), new AdapterSearchInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recSearchInner.setAdapter(adapterSearchInner);


        return view;

    }
}