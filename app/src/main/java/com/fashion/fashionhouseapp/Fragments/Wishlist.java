package com.fashion.fashionhouseapp.Fragments;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fashion.fashionhouseapp.Activity.ItemsDetail;
import com.fashion.fashionhouseapp.Adapter.AdapterWishlist;
import com.fashion.fashionhouseapp.R;

public class Wishlist extends Fragment {

    private RecyclerView recWishList;
    private AdapterWishlist adapterWishlist;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_wishlist, container, false);


        recWishList = view.findViewById(R.id.recWishList);
        recWishList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapterWishlist = new AdapterWishlist(getContext(), new AdapterWishlist.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                startActivity(new Intent(getContext(), ItemsDetail.class));
            }
        });
        recWishList.setAdapter(adapterWishlist);


        return view;
    }


}