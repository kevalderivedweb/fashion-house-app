package com.fashion.fashionhouseapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.fashion.fashionhouseapp.Adapter.AdapterDesigners;
import com.fashion.fashionhouseapp.R;

public class Designers extends AppCompatActivity {

    private RecyclerView recListDesigner;
    private AdapterDesigners adapterDesigners;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_designers);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        findViewById(R.id.refineDesigners).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Designers.this, Refine.class));
            }
        });


        recListDesigner = findViewById(R.id.recListDesigner);
        recListDesigner.setLayoutManager(new LinearLayoutManager(Designers.this));
        adapterDesigners = new AdapterDesigners(Designers.this, new AdapterDesigners.OnItemClickListener() {
            @Override
            public void onItemClick(int position, int item) {
                startActivity(new Intent(Designers.this, DesignersInner.class));
            }
        });
        recListDesigner.setAdapter(adapterDesigners);


    }
}