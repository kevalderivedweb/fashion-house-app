package com.fashion.fashionhouseapp.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fashion.fashionhouseapp.Fragments.Home;
import com.fashion.fashionhouseapp.Fragments.Me;
import com.fashion.fashionhouseapp.Fragments.Search;
import com.fashion.fashionhouseapp.Fragments.ShoppingBag;
import com.fashion.fashionhouseapp.Fragments.Wishlist;
import com.fashion.fashionhouseapp.R;

public class MainActivity extends AppCompatActivity {

    private LinearLayout navLinear1, navLinear2, navLinear3, navLinear4, navLinear5;
    private ImageView image1, image2, image3, image4, image5;
    private TextView text1, text2, text3, text4, text5;

    private TextView tagAppName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        setFooterNavigation();



    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


    private void setFooterNavigation(){

        navLinear1 = findViewById(R.id.navLinear1);
        navLinear2 = findViewById(R.id.navLinear2);
        navLinear3 = findViewById(R.id.navLinear3);
        navLinear4 = findViewById(R.id.navLinear4);
        navLinear5 = findViewById(R.id.navLinear5);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        text5 = findViewById(R.id.text5);


        tagAppName = findViewById(R.id.tagAppName);


        Home home = new Home();
        replaceFragment(R.id.fragmentLinearHome, home, "home", null);


        navLinear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Home home = new Home();
                replaceFragment(R.id.fragmentLinearHome, home, "home", null);

                image1.setImageResource(R.drawable.home_active);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.black));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.VISIBLE);
            }
        });

        navLinear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Search search = new Search();
                replaceFragment(R.id.fragmentLinearHome, search, "search", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search_active);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);



                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.black));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));



                tagAppName.setVisibility(View.GONE);
            }
        });

        navLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Wishlist wishlist = new Wishlist();
                replaceFragment(R.id.fragmentLinearHome, wishlist, "wishlist", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist_active);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.black));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));



                tagAppName.setVisibility(View.GONE);
            }
        });

        navLinear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Me me = new Me();
                replaceFragment(R.id.fragmentLinearHome, me, "me", null);

                image1.setImageResource(R.drawable.home);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user_active);
                image5.setImageResource(R.drawable.bag);


                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.black));
                text5.setTextColor(getResources().getColor(R.color.dark_gray));


                tagAppName.setVisibility(View.GONE);
            }
        });


        navLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ShoppingBag shoppingBag = new ShoppingBag();
                replaceFragment(R.id.fragmentLinearHome, shoppingBag, "shopping_bag", null);

                image1.setImageResource(R.drawable.home_active);
                image2.setImageResource(R.drawable.search);
                image3.setImageResource(R.drawable.wishlist);
                image4.setImageResource(R.drawable.user);
                image5.setImageResource(R.drawable.bag_active);

                text1.setTextColor(getResources().getColor(R.color.dark_gray));
                text2.setTextColor(getResources().getColor(R.color.dark_gray));
                text3.setTextColor(getResources().getColor(R.color.dark_gray));
                text4.setTextColor(getResources().getColor(R.color.dark_gray));
                text5.setTextColor(getResources().getColor(R.color.black));

                tagAppName.setVisibility(View.VISIBLE);
            }
        });


    }

}