package com.fashion.fashionhouseapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fashion.fashionhouseapp.Adapter.AdapterMightAlsoLike;
import com.fashion.fashionhouseapp.Adapter.AdapterNewIn;
import com.fashion.fashionhouseapp.Adapter.ViewPagerProduct;
import com.fashion.fashionhouseapp.Fragments.ShowAllItems;
import com.fashion.fashionhouseapp.Model.ServiceModel;
import com.fashion.fashionhouseapp.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;

public class ItemsDetail extends AppCompatActivity {

    private ArrayList<ServiceModel> mDatasetServices = new ArrayList<>();
    private DotsIndicator worm_dots_indicator;
    private ViewPagerProduct viewPagerProduct;

    private ImageView aDown, aUp, bDown, bUp, cDown, cUp, dDown, dUp;

    private boolean des = true;
    private boolean size = true;
    private boolean care = true;
    private boolean shipping = true;

    private LinearLayout layoutDes;


    private RecyclerView recMightAlsoLike;
    private AdapterNewIn adapterNewIn;
    private AdapterMightAlsoLike adapterMightAlsoLike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_detail);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPager);
        worm_dots_indicator = (DotsIndicator) findViewById(R.id.worm_dots_indicator);
        viewPagerProduct = new ViewPagerProduct(ItemsDetail.this, mDatasetServices, new ViewPagerProduct.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        mViewPager.setAdapter(viewPagerProduct);
        worm_dots_indicator.setViewPager(mViewPager);




        findViewById(R.id.selectSize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetDialog dialog = new BottomSheetDialog(ItemsDetail.this);
                dialog.setContentView(R.layout.bottom_dialog_select_size);
                dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });




        recMightAlsoLike = findViewById(R.id.recMightAlsoLike);
        recMightAlsoLike.setLayoutManager(new LinearLayoutManager(ItemsDetail.this, LinearLayoutManager.HORIZONTAL, false));
        adapterMightAlsoLike = new AdapterMightAlsoLike(ItemsDetail.this, new AdapterMightAlsoLike.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recMightAlsoLike.setAdapter(adapterMightAlsoLike);





     /*   findViewById(R.id.showAllText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ItemsDetail.this, ShowAllItems.class));
            }
        });*/


        /*
        aDown = findViewById(R.id.aDown);
        aUp = findViewById(R.id.aUp);
        bDown = findViewById(R.id.bDown);
        bUp = findViewById(R.id.bUp);
        cDown = findViewById(R.id.cDown);
        cUp = findViewById(R.id.cUp);
        dDown = findViewById(R.id.dDown);
        dUp = findViewById(R.id.dUp);

        layoutDes = findViewById(R.id.layoutDes);



        findViewById(R.id.relative1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (des){

                    aDown.setVisibility(View.GONE);
                    aUp.setVisibility(View.VISIBLE);
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);

                    layoutDes.setVisibility(View.VISIBLE);


                    des = false;
                    size = true;
                    care = true;
                    shipping = true;

                } else {
                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);

                    layoutDes.setVisibility(View.GONE);


                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });

        findViewById(R.id.relative2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (size){

                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);
                    bDown.setVisibility(View.GONE);
                    bUp.setVisibility(View.VISIBLE);
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);


                    des = true;
                    size = false;
                    care = true;
                    shipping = true;

                } else {
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);

                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });

        findViewById(R.id.relative3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (care){

                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);
                    cDown.setVisibility(View.GONE);
                    cUp.setVisibility(View.VISIBLE);
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);


                    des = true;
                    size = true;
                    care = false;
                    shipping = true;

                } else {
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);

                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });

        findViewById(R.id.relative4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shipping){

                    aDown.setVisibility(View.VISIBLE);
                    aUp.setVisibility(View.GONE);
                    bDown.setVisibility(View.VISIBLE);
                    bUp.setVisibility(View.GONE);
                    cDown.setVisibility(View.VISIBLE);
                    cUp.setVisibility(View.GONE);
                    dDown.setVisibility(View.GONE);
                    dUp.setVisibility(View.VISIBLE);


                    des = true;
                    size = true;
                    care = true;
                    shipping = false;

                } else {
                    dDown.setVisibility(View.VISIBLE);
                    dUp.setVisibility(View.GONE);

                    des = true;
                    size = true;
                    care = true;
                    shipping = true;
                }
            }
        });*/



    }
}