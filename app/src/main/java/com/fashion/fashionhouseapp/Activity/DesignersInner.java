package com.fashion.fashionhouseapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fashion.fashionhouseapp.Adapter.AdapterDesignerInner;
import com.fashion.fashionhouseapp.R;

public class DesignersInner extends AppCompatActivity {

    private RecyclerView recDesignerInner;
    private AdapterDesignerInner adapterDesignerInner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_designers_inner);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        findViewById(R.id.refineDesigners).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DesignersInner.this, Refine.class));
            }
        });


        recDesignerInner = findViewById(R.id.recDesignerInner);
        recDesignerInner.setLayoutManager(new GridLayoutManager(DesignersInner.this, 2));
        adapterDesignerInner = new AdapterDesignerInner(this, new AdapterDesignerInner.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(DesignersInner.this, ItemsDetail.class));
            }
        });
        recDesignerInner.setAdapter(adapterDesignerInner);


    }
}